from ete3 import Tree, TreeStyle, TextFace
t = Tree()
t.populate(30)
ts = TreeStyle()
ts.show_leaf_name = True
ts.title.add_face(TextFace("JOKENPO", fsize=40), column=0)
ts.mode = "c"
ts.arc_start = -180 # 0 degrees = 3 o'clock
ts.arc_span = 180
t.show(tree_style=ts)