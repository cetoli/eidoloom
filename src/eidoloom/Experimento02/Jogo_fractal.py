import pygame

pygame.init()




def drawFractalRect(screen, A, B, C, D, alpha, quantity):
    if quantity == 0:
        return
    for M, N in (A, B), (B, C), (C, D), (D, A):
        pygame.draw.line(screen, (255, 2, 55), M, N)


    A0 = (A[0] * (1 - alpha) + B[0] * alpha, A[1] * (1 - alpha) + B[1]* alpha)
    B0 = (B[0] * (1 - alpha) + C[0] * alpha, B[1] * (1 - alpha) + C[1]* alpha)
    C0 = (C[0] * (1 - alpha) + D[0] * alpha, C[1] * (1 - alpha) + D[1]* alpha)
    D0 = (D[0] * (1 - alpha) + A[0] * alpha, D[1] * (1 - alpha) + A[1]* alpha)
    drawFractalRect(screen, A0, B0, C0, D0, alpha, quantity -1)

run = True
while run:


    for i in range (200):
        #pygame.time.delay(10)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.quit()

        if not run:
            break


        screen = pygame.display.set_mode((700, 700))
        drawFractalRect(screen, (100+i,100), (300,100+i), (300-i, 300), (100, 300-i), 0.2, 40)
        drawFractalRect(screen, (400+i,100), (600,100+i), (600-i, 300), (400, 300-i), 0.2, 40)
        drawFractalRect(screen, (400+i,400), (600,400+i), (600-i, 600), (400, 600-i), 0.2, 40)
        drawFractalRect(screen, (100+i,400), (300,400+i), (300-i, 600), (100, 600-i), 0.2, 40)
        drawFractalRect(screen, (250+i,250), (450,250+i), (450-i, 450), (250, 450-i), 0.2, 40)
        pygame.display.update()