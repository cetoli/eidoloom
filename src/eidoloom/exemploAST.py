import ast
#import random as rd
#from random import randint

tree = ast.parse('''
fruits = ['grapes', 'mango']
volatil = {'1':'3'}
aset = set(1,2)
name = 'peter'

for fruit in fruits:
    print('{} likes {}'.format(name, fruit))

with open('x') as xf:
    pass

assert isinstance(name, str)
del volatil


class Fruit:
    def __init__(self):
        try:
            z = [x for x in range(1)]
        except Exception as ex:
            raise TypeError(ex)
        finally:
            pass
        return z

    @classmethod
    def go(cls, x, y=1, *args, **kwargs):
        global name
        nonlocal fruits
        z = 0 if y > 1 else -1
        while z in range(2):
            z += 1
            if x :
              continue
            elif z >=3:
              break
        if name is None:
           name = True
           fruits = {k: False for k in u"xy"}

           ww = (1, 2)
           print(i for i in range(4))
           zw = 1 > 2.0 < 2e1 or 3 == 5 and 3 <= 4 <= 5 or False or not True
        yield z

yy = lambda: 1 * 3 ** 4 + 2 - (5 / 2 // 1) % 3
''')
print(ast.dump(tree))