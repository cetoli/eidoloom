import ast
tree = ast.parse('''from random import choice
from time import sleep

print('='*52)
print('{:^52}'.format('QUERO VER VOCÊ ME GANHAR NO JOKENPO'))
print('='*52)

print('Escolha Pedra, Papel ou Tesoura')
eu = str(input('Jogada: ')).lower()

lista = ['pedra','papel','tesoura']
comp = choice(lista)

if eu == comp:
    status = 'EMPATE'
elif eu != comp:
    if eu == 'pedra':
        if comp == 'tesoura':
            status = 'VOCÊ GANHOU'
        if comp == 'papel':
            status = 'O COMPUTADOR GANHOU'
    if eu == 'tesoura':
        if comp == 'papel':
            status = 'VOCÊ GANHOU'
        if comp == 'pedra':
            status = 'O COMPUTADOR GANHOU'
    if eu == 'papel':
        if comp == 'pedra':
            status = 'VOCÊ GANHOU'
        if comp == 'tesoura':
            status = 'O COMPUTADOR GANHOU'
            ''')
#assert, break, class, continue, def, del, elif, else, except, exec, finally, for, from, global, if, import, in, is, lambda, nonlocal, not, or, pass, raise, return, try, while, with, yeld, true, false, none''')

#code = ast.parse("print('Hello world!')")
#print(code)
#exec(compile(code, filename="", mode="exec"))
#print(ast.dump(code))

TOKENS = dict(
    Module='▲', Assign='◉', Name='▲', Store='▣', List='▣', Str='◱', Load='◬', ClassDef='◪',
    FunctionDef='◨', arguments='▵', arg='▵', Expr='◉', ListComp='◈', comprehension='◈', Call='●',
    Num='◳', For='◠', Attribute='◭', Import='◡', alias='◡', ImportFrom='◡', Dict='▣', With='◡',
    ExceptHandler='◔', Raise='◔', Pass='◐', Return='●', Global='◭', Nonlocal='◭', IfExp='▬', Compare='◎',
    Gt='▱', UnaryOp='◉', USub='▲', While='◠', In='◎', AugAssign='◉', Add='■', If='▬', Continue='◐', GtE='▰',
    Break='◐', Is='◎', NameConstant='▲', DictComp='◈', JoinedStr='◲', FormattedValue='◲', Tuple='▣',
    GeneratorExp='◈', Yield='●', BoolOp='◻', Or='◻', Lt='▱', And='◻', Eq='◻', LtE='▰', Not='◻',
    Assert='◕', Delete='◕', Del='◕', Lambda='◨', BinOp='■', Mult='■', Pow='■', Sub='■', Div='■', FloorDiv='■', Mod='■'
)

class GenV(ast.NodeTransformer):
    def __init__(self):
        self.tokens = {}
        self.gram = ""

    def generic_visit(self, node):
        token_name = type(node).__name__
        self.tokens[token_name] = "\033[1;30m" + u"\u25B2" + "\033[1;0m"
        if token_name in TOKENS:
            self.gram += TOKENS[token_name]
        ast.NodeVisitor.generic_visit(self, node)

    def tokenize(self, text):
        self.visit(ast.parse(text))
        return list(self.gram)



class NodeVisitor(ast.NodeTransformer):
    def visit_Str(self, tree_node):
        print('◱{}'.format(tree_node.s))

    def visit_Name(self, tree_node):
        print('△{}'.format(tree_node.id))

    def visit_Assign(self, tree_node):
        print('◉{}'.format(list(tree_node.targets)[0].id))
        self.visit(tree_node.value)

    def visit_For(self, tree_node):
        print('◠{}'.format(tree_node.target.id))
        [self.visit(node) for node in tree_node.body]

    def visit_Call(self, tree_node):
        print('●{}'.format(tree_node.func.id))
        self.visit(tree_node.func)

    def visit_Attribute(self, tree_node):
        print('◭{}'.format(tree_node.value.s))
        [self.visit(node) for node in tree_node.args]

    def visit_ClassDef(self, tree_node):
        print('◪{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]

    def visit_FunctionDef(self, tree_node):
        print('◨{}'.format(tree_node.name))
        [self.visit(node) for node in tree_node.body]



gv = GenV()
gv.visit(tree)
for tok in gv.tokens:
    print("{}='{}',".format(tok, gv.tokens[tok]), end=' ')
print("toks")

for tok in gv.gram:
    print("{}".format(tok), end='')

